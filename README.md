# gocd (Bash)
The Bash command `gocd` allows to change working directory
from a list of association `NAME`: `DIRECTORY`
read dynamically from configuration files,
with **autocomplete**.

Basically it is an **alternative to usual `cd` command**
that takes first a `NAME` as a **shortcut association** to a defined `DIRECTORY`.

**See workaround below to use it in sh, ksh, zsh…**

To show use and options do (or see below):

```bash
$ gocd --help
```


## Installation of `gocd` command
To set the `gocd` command in Bash,
download both scripts
[`gocd.sh`](https://bitbucket.org/OPiMedia/gocd-bash/src/master/gocd.sh) and
[`enable_gocd.sh`](https://bitbucket.org/OPiMedia/gocd-bash/src/master/enable_gocd.sh)
in `YOUR_DIRECTORY/`
and do:

```bash
$ source YOUR_DIRECTORY/enable_gocd.sh
```

To set it permanently add the previous line in your `~/.bashrc` file.

To suppress the "installation" message
you can redirect the standard output to `/dev/null` like this:

```bash
$ source YOUR_DIRECTORY/enable_gocd.sh > /dev/null
```


To unset `gocd` command, do:

```bash
$ source YOUR_DIRECTORY/enable_gocd.sh (-d|--disable)
```


To create an example of configuration file `"~/.gocd_associations.txt"` if doesn't exist yet, do:

```bash
$ YOUR_DIRECTORY/enable_gocd.sh -c
```


## Usage of `gocd` command

```bash
gocd [-nqx] [NAME [SUBDIR [LS-PARAMS]]]
```

Changes the working directory with ``cd DIRECTORY/SUBDIR``
and then runs ``ls LS-PARAMS``.

If `NAME` is empty string then moves to `~/` (as with `cd`)
else moves to `DIRECTORY` (or its `SUBDIR` subdirectory)
with `DIRECTORY` corresponding to `NAME` found in configuration files
`"~/.gocd_associations.txt"` and `".gocd_associations.txt"` (see below).
If there is no association for `NAME` then sets `DIRECTORY` with `NAME`.


### Options
* `-n`   just prints target directory, doesn't change working directory
         (`ls` runs in the working directory, not in the target)
* `-q`   quiet, doesn't run `ls` at the end
* `-x`   ends by an exit (used by sh, ksh, zsh... workaround)

#### Other options
```bash
gocd (-l|--list|-h|--help|--version)
```

* `-l`, `--list`   prints associations found in configuration files and exit
* `-h`, `--help`   prints this help and exit
* `--version`      prints version information and exit


### Configuration files
`"~/.gocd_associations.txt"` and `".gocd_associations.txt"`
content this kind of associations `NAME`: `DIRECTORY`
(like in this example file
[`.gocd_associations.txt`](https://bitbucket.org/OPiMedia/gocd-bash/src/master/.gocd_associations.txt)):

```
# comment
 Downloads ~/Downloads
git ~/data/git
ParSigmaOdd ~/data/git/current/parallel-sigma_odd-problem
```

* `"~/.gocd_associations.txt"` is read first,
  then content of `".gocd_associations.txt"` overloads previous associations.
* Both of them are read dynamically during the invocation of the command.
* `NAME` is the first word until a space or a tab.
* `DIRECTORY` is the remain of the line.
* Starting and ending spaces are trimmed in `NAME` and `DIRECTORY`.
* When `DIRECTORY` is empty string, eventual previous association for `NAME` is unset.
* When a line starts by a space the association is set
  but ignored by autocomplete.
* Empty lines and lines starting by `#` are ignored.

⚠️ The correct name of the configuration file starts by a dot `"."`.
Be careful if you download the example file
[`.gocd_associations.txt`](https://bitbucket.org/OPiMedia/gocd-bash/src/master/.gocd_associations.txt)
that your browser doesn't erase this initial dot.


### Examples of use
With configuration file content as above:

* ``$ gocd git``
  is equivalent to run
  ``$ cd ~/data/git; ls``
* ``$ gocd git archives/library/assertopencl/``
  is equivalent to run
  ``$ cd ~/data/git/archives/library/assertopencl/; ls``
* ``$ gocd git archives/library/simpleguics2pygame/ -lA``
  is equivalent to run
  ``$ cd ~/data/git/archives/library/simpleguics2pygame/; ls -lA``
* ``$ gocd -``
  is equivalent to run
    ``$ cd -; ls``
* ``$ gocd -nq ParSigmaOdd progs/``
  just prints (even if this path doesn't exist)
  `$HOME/data/git/current/parallel-sigma_odd-problem/progs/`
* ``$ cat $(gocd -nq ParSigmaOdd README.md)``
  is equivalent to run
  ``$ cat $HOME/data/git/current/parallel-sigma_odd-problem/README.md``


### Autocomplete
A nice feature is that Bash autocomplete helps you when you write parameters,
as the `cd` command except that propositions for the first parameter
are get from the list of `NAME`.
Also propositions for the second part `SUBDIR`
are get as if the working directory was already `DIRECTORY` associated to `NAME`.

For example, if you press tabulation key after written ``$ gocd gi``
autocomplete gives you ``$ gocd git ``.
A second press of tabulation key proposes to you
the content of the `~/data/git/` directory.


### Workaround to use `gocd` in sh, ksh, zsh…
`gocd` is written with Bash functionalities, and so doesn't work with other shells.
But here two workarounds to use it with sh, ksh, zsh…

#### Direct usage of `gocd.sh` script
You can run directly the `gocd.sh` script
instead run the `gocd` command (in fact a Bash function).

``$ gocd.sh git``
is equivalent to run
``$ gocd git``
but in a **child Bash** and immediately quit.
So in fact there is no change of the working directory.

But ``$ gocd.sh -nq git``
is really equivalent to run
``$ gocd -nq git``.
Both print
`$HOME/data/git`.

And you can combine this result with the usual `cd` command like this:
``$ cd $(gocd.sh -nq git)``.

That works in all possible shells since `gocd.sh` runs it own Bash.
This is useful to use in some scripts.

But in this way the autocomplete doesn't work.

#### Run temporary interactive Bash inside sh, ksh, zsh…
After the installation of the Bash command `gocd` as explained above,
install a fake version of `gocd` in sh by:

```sh
$ . YOUR_DIRECTORY/enable_gocd_other_shell.sh
```

Or in ksh, zsh… by:

```ksh
$ source YOUR_DIRECTORY/enable_gocd_other_shell.sh
```

To set that permanently add previous lines in your `~/.shrc`, `~/.kshrc`, `~/.zshrc`… files.

Now, for example in ksh, you can run the fake command `gocd` (without parameter):
``$ gocd``

That runs an interactive Bash.
Inside this Bash you can runs the real `gocd` command.
After that this Bash is automatically closed and the working directory in ksh is changed.

```bash
<<< TEMPORARY BASH to run gocd command or exit:
TMP BASH:$ gocd git
end TEMPORARY BASH >>>
```

There is some limitations. You can't use options of the real `gocd` command
(because options `-nqx` are automatically added).
But in the interactive Bash you can use autocomplete.



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## Support me
This program is a **free software** (GPL license).
It is **completely free** (like "free speech" *and like "free beer").
However you can **support me** financially by donating.

Click to this link
[![Donate](http://www.opimedia.be/_png/donate--76x47-t.png)](http://www.opimedia.be/donate/)
**Thank you!**



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2020-2021, 2023 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



## Changes

* November 19, 2023: Few cleaning.
* November 11, 2023: Few cleaning.
* December 8, 2021: Corrected bug about `IFS` expansion.
* November 14, 2021:

    - Split the command and its enabling in two scripts.
    - Added the possibility to run the script `gocd.sh`.
    - Corrected bug when `subdir` is an absolute path.
    - Cleaned `IFS` and `${!GOCD_ASSOCIATIONS[*]}` expansion.
    - Improved `<<<` redirection.

* November 6, 2021: Added use of `checkbashisms` in test script.
* November 4, 2021:

    - Split the command and its enabling in two scripts.
    - Added the possibility to run the script `gocd.sh`.
    - Added `-x` option.
    - Added workaround to use `gocd` in sh, ksh, zsh…

* October 29, 2021: Cleaned Bash style and removed tr dependency.
* August 16, 2020: Minor cleaning about local variable.
* August 15, 2020: Minor cleanings.
* August 14, 2020:

    - Renamed `go` to `gocd`, to avoid name conflict with [Go](https://golang.org/) compiler.
    - Corrected autocomplete of `subdir` with special case `-` as `basedir`.
    - Added `-n` (just print) and `-q` (quiet) options.

* July 28, 2020: First public version.
* Started July 25, 2020.



![gocd](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Aug/13/1654986466-1-gocd-bash-logo_avatar.png)

(logo built from this icon [Commandline, prompt, shell, terminal icon](https://www.iconfinder.com/icons/22281/commandline_prompt_shell_terminal_icon))

#gocd #gocdBash



## Other personal Bash free softwares
* [etoolbox](https://bitbucket.org/OPiMedia/etoolbox/):
  Various scripts.
* [HackerRank [CodinGame…] / helpers](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  Help in solving problems of HackerRank website (or CodinGame…), in several programming languages.
* [proc_deleted (Bash)](https://bitbucket.org/OPiMedia/proc_deleted-bash/):
  List processes that have some deleted files, and allow to kill them.
* [vNu-scripts](https://bitbucket.org/OPiMedia/vnu-scripts/):
  Shell scripts to run *the Nu Html Checker (v.Nu)* to valid (X)HTML files by identifying if each file is a (X)HTML file and which version of (X)HTML.
