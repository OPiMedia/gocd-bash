Test results of gocd --- Sun 19 Nov 2023 04:05:34 PM CET

ShellCheck - shell script analysis tool version: 0.8.0 ----------
$ shellcheck gocd.sh
----------
$ shellcheck enable_gocd.sh
----------
$ shellcheck enable_gocd_other_shell.sh
----------
$ shellcheck TEST_gocd.sh
----------
$ checkbashisms enable_gocd_other_shell.sh

=== Makes "$HOME/.gocd_associations.txt" unreadable ===

=== Checks gocd.sh script function ===
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh --version
gocd (November 19, 2023)
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh --help
Usage: gocd [-nqx] [NAME [SUBDIR [LS-PARAMS]]]
    or gocd (-l|--list|-h|--help|--version)
  Changes the working directory with cd DIRECTORY/SUBDIR
  and then runs ls LS-PARAMS.

  If NAME is empty string then moves to ~/ (as with cd)
  else moves to DIRECTORY (or its SUBDIR subdirectory)
  with DIRECTORY corresponding to NAME found in configuration files
  "~/.gocd_associations.txt" and ".gocd_associations.txt" (see below).
  If there is no association for NAME then sets DIRECTORY with NAME.

Options:
  -n   just prints target directory, doesn't change working directory
         (ls runs in the working directory, not in the target)
  -q   quiet, doesn't run ls at the end
  -x   ends by an exit (used by sh, ksh, zsh... workaround)

Other options:
  -l, --list   prints associations found in configuration files and exit
  -h, --help   prints this help and exit
  --version    prints version information and exit

Configuration files content this kind of associations NAME: DIRECTORY
# comment
 Downloads ~/Downloads
git ~/data/git
ParSigmaOdd ~/data/git/current/parallel-sigma_odd-problem

  "~/.gocd_associations.txt" is read first,
    then content of ".gocd_associations.txt" overloads previous associations.
  Both of them are read dynamically during the invocation of the command.
  NAME is the first word until a space or a tab.
  DIRECTORY is the remain of the line.
  Starting and ending spaces are trimmed in NAME and DIRECTORY.
  When DIRECTORY is empty string,
    eventual previous association for NAME is unset.
  When a line starts by a space the association is set
    but ignored by autocomplete.
  Empty lines and lines starting by # are ignored.

Examples of use (with configuration file content as above):
  $ gocd git
    is equivalent to run
    $ cd ~/data/git; ls
  $ gocd git archives/library/assertopencl/
    is equivalent to run
    $ cd ~/data/git/archives/library/assertopencl/; ls
  $ gocd git archives/library/simpleguics2pygame/ -lA
    is equivalent to run
    $ cd ~/data/git/archives/library/simpleguics2pygame/; ls -lA
  $ gocd -
    is equivalent to run
    $ cd -; ls
  $ gocd -nq ParSigmaOdd progs/
    just prints (even if this path doesn't exist)
    $HOME/data/git/current/parallel-sigma_odd-problem/progs/
  $ cat $(gocd -nq ParSigmaOdd README.md)
    is equivalent to run
    $ cat $HOME/data/git/current/parallel-sigma_odd-problem/README.md

Exist status:
  Returns 0 if successful, else returns 1.

Requirements: cat, ls and sort.

Latest version and more explanations on Bitbucket:
https://bitbucket.org/OPiMedia/gocd-bash/

GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
http://www.opimedia.be/
Current version: November 19, 2023

WARNING!
You ran this command by directly calling this script,
so current directory will be NOT changed in caller terminal.
Install the gocd command by:
$ source enable_gocd.sh
See explanation on the Bitbucket repository.
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh --list
bak                 $HOME/bak
current             $HOME/current
data                $HOME/data
Downloads           $HOME/Downloads
ework               $HOME/ework
git                 $HOME/data/git
OPiLaTeXWORK        $HOME/data/git/WORK/opilatex__WORK/src
opimedia            $HOME/data/opimedia
OutCmpWORK          $HOME/data/git/WORK/outcmp__WORK
ParSigmaOdd         $HOME/data/git/current/parallel-sigma_odd-problem
ParSigmaOddWORK     $HOME/data/git/WORK/parallel-sigma_odd-problem__WORK
SlidesFromVideoWORK $HOME/data/git/WORK/slidesfromvideo__WORK
TSVWORK             $HOME/data/git/WORK/tsv2htmltable__WORK/src
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh git archives/prog/gocd-bash
COPYING
enable_gocd_other_shell.sh
enable_gocd.sh
gocd.sh
_img
README.md
TEST_gocd_correct_output.txt
TEST_gocd_output.txt
TEST_gocd.sh
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -q git archives/prog/
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -nq git archives/prog/gocd-bash/COPYING
$HOME/data/git/archives/prog/gocd-bash/COPYING
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -n -q git archives/prog/gocd-bash/COPYING
$HOME/data/git/archives/prog/gocd-bash/COPYING
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -q /var/tmp/
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -nq /var/tmp/
/var/tmp/
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh git WRONG
./gocd.sh: line fake_line: cd: $HOME/data/git/WRONG: No such file or directory
?=1
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh NAME WRONG
./gocd.sh: line fake_line: cd: NAME/WRONG: No such file or directory
?=1
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ ./gocd.sh -
COPYING
enable_gocd_other_shell.sh
enable_gocd.sh
gocd.sh
_img
README.md
TEST_gocd_correct_output.txt
TEST_gocd_output.txt
TEST_gocd.sh
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------

=== Enables gocd Bash function ===
$ source ./enable_gocd.sh
gocd (November 19, 2023) command enabled: to show options run $ gocd --help
----------
$ ./enable_gocd.sh --help
Usage: source enable_gocd.sh [-d|--disable]
    or enable_gocd.sh (-c|-h|--help)

Sets a "gocd" command in Bash by
$ source enable_gocd.sh
or unsets it by
$ source enable_gocd.sh (-d|--disable)

$ enable_gocd.sh -c
If configuration file "~/.gocd_associations.txt" already exists,
then prints it,
else creates it with an example content and prints it.

$ enable_gocd.sh (-h|--help)
prints this help.

This Bash command "gocd" allows to change working directory
from a list of association NAME: DIRECTORY
read dynamically from configuration files,
with autocomplete.

To show use and options (when the command was enabled):
$ gocd --help

Requirements:.
* autocomplete _cd function
  normally defined in "/etc/bash_completion" or "/etc/bash_completion.d"
* cat external command.

Latest version and more explanations on Bitbucket:
https://bitbucket.org/OPiMedia/gocd-bash/

GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
http://www.opimedia.be/
./enable_gocd.sh

=== Checks gocd Bash function ===
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd --version
gocd (November 19, 2023)
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd --help
Usage: gocd [-nqx] [NAME [SUBDIR [LS-PARAMS]]]
    or gocd (-l|--list|-h|--help|--version)
  Changes the working directory with cd DIRECTORY/SUBDIR
  and then runs ls LS-PARAMS.

  If NAME is empty string then moves to ~/ (as with cd)
  else moves to DIRECTORY (or its SUBDIR subdirectory)
  with DIRECTORY corresponding to NAME found in configuration files
  "~/.gocd_associations.txt" and ".gocd_associations.txt" (see below).
  If there is no association for NAME then sets DIRECTORY with NAME.

Options:
  -n   just prints target directory, doesn't change working directory
         (ls runs in the working directory, not in the target)
  -q   quiet, doesn't run ls at the end
  -x   ends by an exit (used by sh, ksh, zsh... workaround)

Other options:
  -l, --list   prints associations found in configuration files and exit
  -h, --help   prints this help and exit
  --version    prints version information and exit

Configuration files content this kind of associations NAME: DIRECTORY
# comment
 Downloads ~/Downloads
git ~/data/git
ParSigmaOdd ~/data/git/current/parallel-sigma_odd-problem

  "~/.gocd_associations.txt" is read first,
    then content of ".gocd_associations.txt" overloads previous associations.
  Both of them are read dynamically during the invocation of the command.
  NAME is the first word until a space or a tab.
  DIRECTORY is the remain of the line.
  Starting and ending spaces are trimmed in NAME and DIRECTORY.
  When DIRECTORY is empty string,
    eventual previous association for NAME is unset.
  When a line starts by a space the association is set
    but ignored by autocomplete.
  Empty lines and lines starting by # are ignored.

Examples of use (with configuration file content as above):
  $ gocd git
    is equivalent to run
    $ cd ~/data/git; ls
  $ gocd git archives/library/assertopencl/
    is equivalent to run
    $ cd ~/data/git/archives/library/assertopencl/; ls
  $ gocd git archives/library/simpleguics2pygame/ -lA
    is equivalent to run
    $ cd ~/data/git/archives/library/simpleguics2pygame/; ls -lA
  $ gocd -
    is equivalent to run
    $ cd -; ls
  $ gocd -nq ParSigmaOdd progs/
    just prints (even if this path doesn't exist)
    $HOME/data/git/current/parallel-sigma_odd-problem/progs/
  $ cat $(gocd -nq ParSigmaOdd README.md)
    is equivalent to run
    $ cat $HOME/data/git/current/parallel-sigma_odd-problem/README.md

Exist status:
  Returns 0 if successful, else returns 1.

Requirements: cat, ls and sort.

Latest version and more explanations on Bitbucket:
https://bitbucket.org/OPiMedia/gocd-bash/

GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
http://www.opimedia.be/
Current version: November 19, 2023
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd --list
bak                 $HOME/bak
current             $HOME/current
data                $HOME/data
Downloads           $HOME/Downloads
ework               $HOME/ework
git                 $HOME/data/git
OPiLaTeXWORK        $HOME/data/git/WORK/opilatex__WORK/src
opimedia            $HOME/data/opimedia
OutCmpWORK          $HOME/data/git/WORK/outcmp__WORK
ParSigmaOdd         $HOME/data/git/current/parallel-sigma_odd-problem
ParSigmaOddWORK     $HOME/data/git/WORK/parallel-sigma_odd-problem__WORK
SlidesFromVideoWORK $HOME/data/git/WORK/slidesfromvideo__WORK
TSVWORK             $HOME/data/git/WORK/tsv2htmltable__WORK/src
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd git archives/prog/gocd-bash
COPYING
enable_gocd_other_shell.sh
enable_gocd.sh
gocd.sh
_img
README.md
TEST_gocd_correct_output.txt
TEST_gocd_output.txt
TEST_gocd.sh
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -q git archives/prog/
?=0
PWD=$HOME/data/git/archives/prog
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -nq git archives/prog/gocd-bash/COPYING
$HOME/data/git/archives/prog/gocd-bash/COPYING
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -n -q git archives/prog/gocd-bash/COPYING
$HOME/data/git/archives/prog/gocd-bash/COPYING
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -q /var/tmp/
?=0
PWD=/var/tmp
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -nq /var/tmp/
/var/tmp/
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd git WRONG
./gocd.sh: line fake_line: cd: $HOME/data/git/WRONG: No such file or directory
?=1
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd NAME WRONG
./gocd.sh: line fake_line: cd: NAME/WRONG: No such file or directory
?=1
PWD=$HOME/data/git/archives/prog/gocd-bash
----------
PWD=$HOME/data/git/archives/prog/gocd-bash
$ gocd -
COPYING
enable_gocd_other_shell.sh
enable_gocd.sh
gocd.sh
_img
README.md
TEST_gocd_correct_output.txt
TEST_gocd_output.txt
TEST_gocd.sh
?=0
PWD=$HOME/data/git/archives/prog/gocd-bash
----------

=== Disables gocd Bash function ===
$ source ./enable_gocd.sh -d
gocd command disabled

=== Restores "$HOME/.gocd_associations.txt" readable ===
---------- END ----------
