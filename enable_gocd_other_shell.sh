#
# sh          usage: . enable_gocd_other_shell.sh
# ksh, zsh... usage: source enable_gocd_other_shell.sh
#
# Sets a "gocd" command in sh by
# $ . enable_gocd_other_shell.sh
# or sets a "gocd" command in ksh, zsh by
# $ source enable_gocd_other_shell.sh
#
# This command "gocd" is a workaround
# to use the real Bash command "gocd" in a temporary Bash.
# See explanation on the Bitbucket repository.
#
# Requirement: bash, grep and mktemp
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/gocd-bash/
#
# GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 11, 2023: Few cleaning.
# * November 4, 2021: First public version.
# * Started November 2, 2021
#

# Set default constants if not already exist
if ! (set | grep --quiet '^GOCD_OTHER_SHELL_BEGIN='); then
    GOCD_OTHER_SHELL_BEGIN='<<< TEMPORARY BASH to run gocd command or exit:'
fi

if ! (set | grep --quiet '^GOCD_OTHER_SHELL_END='); then
    GOCD_OTHER_SHELL_END='end TEMPORARY BASH >>>'
fi

if ! (set | grep --quiet '^GOCD_OTHER_SHELL_PS1_PREFIX='); then
    GOCD_OTHER_SHELL_PS1_PREFIX='TMP BASH:'
fi


# Command to open a temporary bash with the real gocd command
gocd() {  # [LS-PARAMS]
    file=$(mktemp -t gocd_tmp.XXXXXXXXXX)

    # shellcheck disable=SC2064
    trap "rm --force $file" EXIT

    {
        cat "$HOME/.bashrc"
        echo "PS1=\"$GOCD_OTHER_SHELL_PS1_PREFIX\$PS1\""
        echo "alias gocd='gocd -nqx'"
    } > "$file"

    if [ -n "$GOCD_OTHER_SHELL_BEGIN" ]; then
        echo "$GOCD_OTHER_SHELL_BEGIN"
    fi

    output=$(bash --init-file "$file")

    if [ -n "$GOCD_OTHER_SHELL_END" ]; then
        echo "$GOCD_OTHER_SHELL_END"
    fi

    last=$(echo "$output" | tail --lines 1)

    case "${last}" in
        /*)
            # shellcheck disable=SC2164
            cd "$last"
            ls "$@"
            ;;
        *)
            if [ -n "$output" ]; then
                echo "$output"
            fi
            ;;
    esac
}
