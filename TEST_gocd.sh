#!/bin/bash

#
# Runs several times scripts and gocd function with some options to check it.
#
# November 19, 2023
#
# GPLv3 --- Copyright (C) 2021, 2023 Olivier Pirson
# http://www.opimedia.be/
#

OUTPUT="$PWD/TEST_gocd_output.txt"

{  # Write stdout and stderr to "$OUTPUT"
    echo "Test results of gocd --- $(date)"

    echo
    # https://github.com/koalaman/shellcheck
    shellcheck --version | head -n 2 | tr '\n' ' '

    for FILE in gocd.sh enable_gocd.sh enable_gocd_other_shell.sh TEST_gocd.sh; do
        echo '----------'
        printf '$ shellcheck %s\n' "$FILE"
        if [[ "$FILE" == 'enable_gocd_other_shell.sh' ]]; then
            shellcheck -s sh "$FILE"
        else
            shellcheck -x "$FILE"
        fi
    done

    echo '----------'
    echo '$ checkbashisms enable_gocd_other_shell.sh'
    FILE=$(mktemp -t enable_gocd_other_shell_tmp.XXXXXXXXXX)
    cat - enable_gocd_other_shell.sh > "$FILE" <<< '#!/bin/sh'
    checkbashisms "$FILE"
    rm -f "$FILE"


    echo
    # shellcheck disable=SC2016
    echo '=== Makes "$HOME/.gocd_associations.txt" unreadable ==='
    chmod a-r "$HOME/.gocd_associations.txt"


    declare -a OPTIONS


    echo
    echo '=== Checks gocd.sh script function ==='
    for LINE in \
        '--version' \
            '--help' \
            \
            '--list' \
            \
            'git archives/prog/gocd-bash' \
            '-q git archives/prog/' \
            '-nq git archives/prog/gocd-bash/COPYING' \
            '-n -q git archives/prog/gocd-bash/COPYING' \
            '-q /var/tmp/' \
            '-nq /var/tmp/' \
            'git WRONG' \
            'NAME WRONG' \
            '-' \
        ; do
        # shellcheck disable=SC2162
        IFS=' ' read -a OPTIONS <<< "$LINE"

        cd "$PWD" || exit 1  # useless move, to prepare the execution of gocd -
        echo "PWD=$PWD"
        echo '$ ./gocd.sh' "${OPTIONS[@]}"

        # Really runs the command (in a subshell to go back to the current directory)
        (
            ./gocd.sh "${OPTIONS[@]}"
            echo "?=$?"
            echo "PWD=$PWD"
        )

        echo '----------'
    done


    echo
    echo '=== Enables gocd Bash function ==='
    echo '$ source ./enable_gocd.sh'
    source ./enable_gocd.sh

    echo '----------'
    echo '$ ./enable_gocd.sh --help'
    ./enable_gocd.sh --help


    echo
    echo '=== Checks gocd Bash function ==='
    for LINE in \
        '--version' \
            '--help' \
            \
            '--list' \
            \
            'git archives/prog/gocd-bash' \
            '-q git archives/prog/' \
            '-nq git archives/prog/gocd-bash/COPYING' \
            '-n -q git archives/prog/gocd-bash/COPYING' \
            '-q /var/tmp/' \
            '-nq /var/tmp/' \
            'git WRONG' \
            'NAME WRONG' \
            '-' \
        ; do
        # shellcheck disable=SC2162
        IFS=' ' read -a OPTIONS <<< "$LINE"

        cd "$PWD" || exit 1  # useless move, to prepare the execution of gocd -
        echo "PWD=$PWD"
        echo '$ gocd' "${OPTIONS[@]}"

        # Really runs the command (in a subshell to go back to the current directory)
        (
            gocd "${OPTIONS[@]}"
            echo "?=$?"
            echo "PWD=$PWD"
        )

        echo '----------'
    done


    echo
    echo '=== Disables gocd Bash function ==='
    if [[ $(type -t gocd) != 'function' ]]; then
        echo "function gocd doesn't exist!"
    fi
    echo '$ source ./enable_gocd.sh -d'

    source ./enable_gocd.sh -d

    if [[ $(type -t gocd) == 'function' ]]; then
        echo 'function gocd exists yet!'
    fi


    echo
    # shellcheck disable=SC2016
    echo '=== Restores "$HOME/.gocd_associations.txt" readable ==='
    chmod a+r "$HOME/.gocd_associations.txt"


    echo '---------- END ----------'
} 2>&1 | tee "$OUTPUT"


# Clean "noise" in the output result
sed -E --in-place \
    -e "s|$HOME|\$HOME|g" \
    -e 's/(: line )[[:digit:]]+(:)/\1fake_line\2/g' \
    "$OUTPUT"


# Compare results
echo "COMPARE $OUTPUT and TEST_gocd_correct_output.txt"
if diff --color <(tail --lines +2 "$OUTPUT") <(tail --lines +2 TEST_gocd_correct_output.txt); then
    echo "$(tput setaf 2)OK$(tput sgr0)"
else
    echo "$(tput setaf 1)DIFFERENT$(tput sgr0)"
fi
