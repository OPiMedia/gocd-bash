#!/bin/bash

#
# Usage: gocd [-nqx] [NAME [SUBDIR [LS-PARAMS]]]
#     or gocd (-l|--list|-h|--help|--version)
#   Changes the working directory with cd DIRECTORY/SUBDIR
#   and then runs ls LS-PARAMS.
#
#   If NAME is empty string then moves to ~/ (as with cd)
#   else moves to DIRECTORY (or its SUBDIR subdirectory)
#   with DIRECTORY corresponding to NAME found in configuration files
#   "~/.gocd_associations.txt" and ".gocd_associations.txt" (see below).
#   If there is no association for NAME then sets DIRECTORY with NAME.
#
# Options:
#   -n   just prints target directory, doesn't change working directory
#          (ls runs in the working directory, not in the target)
#   -q   quiet, doesn't run ls at the end
#   -x   ends by an exit (used by sh, ksh, zsh... workaround)
#
# Other options:
#   -l, --list   prints associations found in configuration files and exit
#   -h, --help   prints this help and exit
#   --version    prints version information and exit
#
# Configuration files content this kind of associations NAME: DIRECTORY
# # comment
#  Downloads ~/Downloads
# git ~/data/git
# ParSigmaOdd ~/data/git/current/parallel-sigma_odd-problem
#
#   "~/.gocd_associations.txt" is read first,
#     then content of ".gocd_associations.txt" overloads previous associations.
#   Both of them are read dynamically during the invocation of the command.
#   NAME is the first word until a space or a tab.
#   DIRECTORY is the remain of the line.
#   Starting and ending spaces are trimmed in NAME and DIRECTORY.
#   When DIRECTORY is empty string,
#     eventual previous association for NAME is unset.
#   When a line starts by a space the association is set
#     but ignored by autocomplete.
#   Empty lines and lines starting by # are ignored.
#
# Examples of use (with configuration file content as above):
#   $ gocd git
#     is equivalent to run
#     $ cd ~/data/git; ls
#   $ gocd git archives/library/assertopencl/
#     is equivalent to run
#     $ cd ~/data/git/archives/library/assertopencl/; ls
#   $ gocd git archives/library/simpleguics2pygame/ -lA
#     is equivalent to run
#     $ cd ~/data/git/archives/library/simpleguics2pygame/; ls -lA
#   $ gocd -
#     is equivalent to run
#     $ cd -; ls
#   $ gocd -nq ParSigmaOdd progs/
#     just prints (even if this path doesn't exist)
#     $HOME/data/git/current/parallel-sigma_odd-problem/progs/
#   $ cat $(gocd -nq ParSigmaOdd README.md)
#     is equivalent to run
#     $ cat $HOME/data/git/current/parallel-sigma_odd-problem/README.md
#
# Exist status:
#   Returns 0 if successful, else returns 1.
#
# Requirements: cat, ls and sort.
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/gocd-bash/
#
# GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 19, 2023: Few cleaning.
# * November 11, 2023: Few cleaning.
# * December 8, 2021: Corrected bug about IFS expansion.
# * November 14, 2021:
#     - Corrected bug when subdir is an absolute path.
#     - Cleaned ${!GOCD_ASSOCIATIONS[*]} expansion.
#     - Improved <<< redirection.
# * November 4, 2021:
#     - Split the command and its enabling in two scripts.
#     - Added the possibility to run the script gocd.sh.
#     - Added -x option.
# * October 29, 2021: Cleaned Bash style and removed tr dependency.
# * August 16, 2020: Minor cleaning about local variable.
# * August 15, 2020: Minor cleanings.
# * August 14, 2020:
#   - Renamed go to gocd, to avoid name conflict with Go compiler.
#   - Corrected autocomplete of subdir with special case - as basedir.
#   - Added -n (just print) and -q (quiet) options.
# * July 28, 2020: First public version.
# * Started July 25, 2020
#

declare -i GOCD_RAN_DIRECTLY=1  # 0 is for true, other is for false

declare -A GOCD_ASSOCIATIONS
declare -A GOCD_RESTRICTS


# Fills GOCD_ASSOCIATIONS and GOCD_RESTRICTS associative arrays with associations
# from configuration files "~/.gocd_associations.txt" and ".gocd_associations.txt"
# (if both exist then "~/.gocd_associations.txt" is read first).
#
# Fills GOCD_ASSOCIATIONS with all associations.
# Fills GOCD_RESTRICTS without associations starting by space in configuration file.
#
# Returns 0 if some associations was found,
# else returns 1.
function _gocd_read_association_files {
    local name
    local directory
    local association

    GOCD_ASSOCIATIONS=()
    GOCD_RESTRICTS=()

    local line
    while IFS='' read -r line; do
        line="${line//$'\t'/ }"

        # Splits line in name and directory
        IFS=' ' read -r name directory <<< "$line"

        if [[ "$name" != '#'* && -n "$name" ]]; then  # not comment and not empty
            if [[ -z "$directory" ]]; then  # removes previous association
                unset "GOCD_ASSOCIATIONS[$name]"
                unset "GOCD_RESTRICTS[$name]"
            else                            # sets association
                association="${directory/#\~/$HOME}"
                GOCD_ASSOCIATIONS["$name"]="$association"
                if [[ "$line" != ' '* ]]; then
                    GOCD_RESTRICTS["$name"]="$association"
                fi
            fi
        fi
    done < <(cat ~/.gocd_associations.txt .gocd_associations.txt 2> /dev/null)

    if [[ "${#GOCD_ASSOCIATIONS[@]}" -eq 0 ]]; then  # is empty
        return 1
    else
        return 0
    fi
}


# Main function
function gocd {  # [all options]
    local -i final_exit=1

    # Prints help message (reads from the script itself).
    function print_help {  # [option]
        local -r wrong_option="$1"

        if [[ $# -ne 0 ]]; then
            {
                echo "gocd: $wrong_option: No such option"
                echo
            } 1>&2
        fi

        local -i skip_begin=0

        local line
        while IFS='' read -r line; do
            if [[ "$line" == '# Usage:'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            if [[ "$line" == '##'* ]]; then break; fi
            printf '%s\n' "${line:2}"
        done < "${BASH_SOURCE[0]}"

        echo "Current version: $(print_version)"

        if [[ $GOCD_RAN_DIRECTLY -eq 0 ]]; then
            echo '
WARNING!
You ran this command by directly calling this script,
so current directory will be NOT changed in caller terminal.
Install the gocd command by:
$ source enable_gocd.sh
See explanation on the Bitbucket repository.'
        fi
    }

    # Prints version (reads it from the script itself, just after the first ## line).
    function print_version {
        local -i skip_begin=0
        local version

        local line
        while IFS='' read -r line; do
            if [[ "$line" == '##'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            line="${line:4}"
            if [[ -n "$line" ]]; then
                IFS=':' read -r version line <<< "$line"
                printf '%s\n' "$version"

                break
            fi
        done < "${BASH_SOURCE[0]}"
    }

    # Encapsulates main work to deal with final exit option.
    function main {  # [all options]
        local -r param="$1"

        # Reads command line parameters and runs corresponding action
        case "$param" in
            -h|--help)
                print_help

                return 0
                ;;

            -l|--list)
                _gocd_read_association_files

                local names
                local -i length
                local -i current_length

                # Gets all names
                IFS=$'\n'
                names=$(sort --ignore-case <<< "${!GOCD_ASSOCIATIONS[*]}")

                # Computes maximal length
                length=0
                local name
                for name in $names; do
                    current_length="${#name}"
                    if [[ -z "${GOCD_RESTRICTS[$name]}" ]]; then  # name started by space in file
                        current_length=$((current_length + 1))
                    fi
                    if [[ $length -lt $current_length ]]; then
                        length=$current_length
                    fi
                done

                # Prints association that they printed by autocomplete
                for name in $names; do
                    if [[ -n "${GOCD_RESTRICTS[$name]}" ]]; then
                        printf "%-${length}s %s\\n" "$name" "${GOCD_ASSOCIATIONS[$name]}"
                    fi
                done

                # Prints association that they ignored by autocomplete
                for name in $names; do
                    if [[ -z "${GOCD_RESTRICTS[$name]}" ]]; then
                        printf "%-${length}s %s\\n" " $name" "${GOCD_ASSOCIATIONS[$name]}"
                    fi
                done

                return 0
                ;;

            --version)
                echo "gocd ($(print_version))"

                return 0
                ;;

            *)  # normal invocation: by default changes directory and then runs ls
                # Sets (and removes) options
                local -i just_print=1
                local -i quiet=1

                local option
                local -i i
                local char

                # Reads options
                while [[ "$1" != '-' && "$1" == '-'* ]]; do
                    option="$1"
                    shift

                    if [[ "$option" == '--'* ]]; then  # unknown parameter
                        print_help "$option" 1>&2

                        return 1
                    fi

                    # Reads each option character
                    for i in $(seq 2 ${#option}); do
                        char="${option:i-1:1}"

                        case "$char" in
                            n)
                                just_print=0
                                ;;
                            q)
                                quiet=0
                                ;;
                            x)
                                final_exit=0
                                ;;
                            *)  # unknown parameter
                                print_help "-$char" 1>&2

                                return 1
                                ;;
                        esac
                    done
                done

                # Sets basedir and subdir
                local fulldir
                local -r name="$1"

                shift

                if [[ -z "$name" ]]; then  # name is empty, gets home directory
                    fulldir="$HOME"
                else                       # name is not empty
                    local subdir="$1"

                    shift

                    # Fills GOCD_ASSOCIATIONS (and GOCD_RESTRICTS)
                    if ! _gocd_read_association_files; then
                        {
                            echo 'gocd: Configuration files not found or empty'
                            echo
                            print_help
                        } 1>&2

                        return 1
                    fi

                    # Sets basedir
                    local basedir="${GOCD_ASSOCIATIONS[$name]}"

                    if [[ -z "$basedir" ]]; then  # no association with name, gets name
                        basedir="$name"
                    fi
                    if [[ "$basedir" == '-' ]]; then  # gets previous current directory
                        basedir="$OLDPWD"
                    fi

                    if [[ -z "$basedir" ]]; then
                        {
                            echo 'gocd: Empty DIRECTORY'
                            echo
                            print_help
                        } 1>&2

                        return 1
                    fi

                    # Sets fulldir
                    if [[ -z "$subdir" ]]; then  # only part given by NAME
                        fulldir="$basedir"
                    else                         # there is also a subdir part
                        if [[ "$subdir" =~ ^/ ]]; then  # the subdir part is absolute
                            fulldir="$subdir"
                        else                            # common case
                            fulldir="$basedir/$subdir"
                        fi
                    fi
                fi

                # Prints target directory or changes working directory
                if [[ "$just_print" -eq 0 ]]; then  # prints target directory (not change directory)
                    printf '%s\n' "$fulldir"
                else                                # changes directory
                    cd "$fulldir" || return 1
                fi

                if [[ "$quiet" -ne 0 ]]; then  # runs ls
                    ls "$@"
                fi
                ;;
        esac

        return 0
    }


    #
    # Runs main work
    #
    main "$@"

    local -i return_code=$?

    if [[ $final_exit -eq 0 ]]; then
        exit $return_code
    else
        return $return_code
    fi
}



if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then  # ran directly, not by sourced function
    GOCD_RAN_DIRECTLY=0
    gocd "$@"

    exit $?
fi
