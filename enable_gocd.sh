#!/bin/bash

#
# Usage: source enable_gocd.sh [-d|--disable]
#     or enable_gocd.sh (-c|-h|--help)
#
# Sets a "gocd" command in Bash by
# $ source enable_gocd.sh
# or unsets it by
# $ source enable_gocd.sh (-d|--disable)
#
# $ enable_gocd.sh -c
# If configuration file "~/.gocd_associations.txt" already exists,
# then prints it,
# else creates it with an example content and prints it.
#
# $ enable_gocd.sh (-h|--help)
# prints this help.
#
# This Bash command "gocd" allows to change working directory
# from a list of association NAME: DIRECTORY
# read dynamically from configuration files,
# with autocomplete.
#
# To show use and options (when the command was enabled):
# $ gocd --help
#
# Requirements:.
# * autocomplete _cd function
#   normally defined in "/etc/bash_completion" or "/etc/bash_completion.d"
# * cat external command.
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/gocd-bash/
#
# GPLv3 --- Copyright (C) 2020-2021, 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 19, 2023: Few cleaning.
# * November 11, 2023: Few cleaning.
# * December 8, 2021: Corrected bug about IFS expansion.
# * November 14, 2021: Cleaned IFS and ${!GOCD_RESTRICTS[*]} expansion.
# * November 4, 2021:
#     - Split the command and its enabling in two scripts.
#     - Added the possibility to run the script gocd.sh.
# * October 29, 2021: Cleaned Bash style and removed tr dependency.
# * August 16, 2020: Minor cleaning about local variable.
# * August 15, 2020: Minor cleanings.
# * August 14, 2020:
#   - Renamed go to gocd, to avoid name conflict with Go compiler.
#   - Corrected autocomplete of subdir with special case - as basedir.
#   - Added -n (just print) and -q (quiet) options.
# * July 28, 2020: First public version.
# * Started July 25, 2020
#

case "$1" in
    -c)
        if [[ -a ~/.gocd_associations.txt ]]; then
            echo 'gocd: Configuration file "~/.gocd_associations.txt" already exist'
        else
            cat > ~/.gocd_associations.txt <<EOF
doc ~/Documents/my/working/doc
Documents ~/Documents
Downloads ~/Downloads
EOF
            echo 'gocd: Configuration file "~/.gocd_associations.txt" created'
        fi

        cat ~/.gocd_associations.txt
        ;;

    -d|--disable)
        if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then  # ran directly
            {
                echo 'Error: Script must be sourced by'
                printf '$ source enable_gocd.sh %s\n' "$1"
            } 1>&2

            exit 1
        fi

        unset GOCD_ASSOCIATIONS
        unset GOCD_RAN_DIRECTLY
        unset GOCD_RESTRICTS
        unset _gocd
        unset gocd
        unset _gocd_read_association_files

        echo "gocd command disabled"
        ;;

    -h|--help)     # prints help message
        # Prints help message (reads from the script itself).
        function print_help {
            local -i skip_begin=0  # 0 is for true, other is for false

            local line
            while IFS='' read -r line; do
                if [[ "$line" == '# Usage:'* ]]; then skip_begin=1; fi
                if [[ "$skip_begin" -eq 0 ]]; then continue; fi
                if [[ "$line" == '##'* ]]; then break; fi
                printf '%s\n' "${line:2}"
            done < "${BASH_SOURCE[0]}"
            printf '%s\n' "${BASH_SOURCE[0]}"
        }

        print_help
        ;;

    '')
        if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then  # ran directly
            {
                echo 'Error: Script must be sourced by'
                echo '$ source enable_gocd.sh'
            } 1>&2

            exit 1
        fi

        # Loads gocd and _gocd_read_association_files functions
        # shellcheck disable=SC1090
        source "$(dirname "${BASH_SOURCE[0]}")/gocd.sh" || return 1

        # Set COMPREPLY with autocomplete possibilities
        function _gocd {
            # Remove options
            while [[ "${COMP_WORDS[1]}" != '-' && "${COMP_WORDS[1]}" == '-'* ]]; do
                COMP_WORDS=("${COMP_WORDS[@]:0:1}" "${COMP_WORDS[@]:2}")
                ((COMP_CWORD -= 1))
            done

            local -r name="${COMP_WORDS[1]}"

            COMPREPLY=()

            case "$COMP_CWORD" in
                1)  # param NAME
                    # Fills GOCD_RESTRICTS (and GOCD_ASSOCIATIONS)
                    _gocd_read_association_files

                    compopt -o bashdefault

                    # Builds propositions
                    # shellcheck disable=SC2207
                    COMPREPLY=( $(IFS=' '; compgen -W "${!GOCD_RESTRICTS[*]}" "$name" 2> /dev/null) )
                    ;;

                2)  # param SUBDIR
                    # Fills GOCD_ASSOCIATIONS (and GOCD_RESTRICTS)
                    _gocd_read_association_files

                    # Sets basedir
                    local basedir="${GOCD_ASSOCIATIONS[$name]}"

                    if [[ -z "$basedir" ]]; then  # no association with name, gets name
                        basedir="$name"
                    fi
                    if [[ "$basedir" == '-' ]]; then  # gets previous current directory
                        basedir="$OLDPWD"
                    fi

                    compopt -o nospace

                    if [[ "$basedir" == '.' ]]; then
                        _cd
                    else
                        # Move to target basedir
                        local -r oldpwd="$OLDPWD"

                        cd "$basedir" 2> /dev/null || return 1

                        # Builds propositions for this current direcoty
                        _cd

                        # Go back to starting current directory
                        cd - > /dev/null || return 1
                        OLDPWD="$oldpwd"  # restore
                    fi

                    # Adds final / characters if not present
                    # FIXME: I don't understand why "gocd ." adds automatically a second / character
                    local -i nb="${#COMPREPLY[@]}"

                    nb=$nb-1

                    local -i i
                    for i in $(seq 0 "$nb"); do
                        if [[ "${COMPREPLY[$i]}" != *'/' ]]; then
                            COMPREPLY[$i]="${COMPREPLY[$i]}/"
                        fi
                    done
                    ;;
            esac

            return 0
        }


        # Init
        GOCD_VERSION=$(gocd --version) || return 1

        complete -F _gocd gocd

        printf '%s command enabled: to show options run $ gocd --help\n' "$GOCD_VERSION"

        unset GOCD_VERSION

        return 0
        ;;

    *)
        echo "enable_gocd.sh: gocd: $1: No such option" 1>&2

        if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then  # ran directly
            exit 1
        else                                        # ran by source
            return 1
        fi
        ;;
esac
